package com.example.castro.examen_uni2;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class ArchivosAdapter extends CursorAdapter {
    public ArchivosAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.mis_datos,viewGroup,false);
    }

    @Override
    //vincular
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tvwusu, tvwape,tvwnom;
        tvwnom=(TextView)view.findViewById(R.id.tvwnom);
        tvwape=(TextView)view.findViewById(R.id.tvwape);
        tvwusu=(TextView)view.findViewById(R.id.tvwusu);
        //enlazamos los datos del cursor a los textview
        String sNom = cursor.getString(cursor.getColumnIndexOrThrow("nombre"));
        String sApe = cursor.getString(cursor.getColumnIndexOrThrow("apellido"));
        String sUsu = cursor.getString(cursor.getColumnIndexOrThrow("usuario"));
        //
        tvwnom.setText(sNom);
        tvwape.setText(sApe);
        tvwusu.setText(sUsu);
    }
}