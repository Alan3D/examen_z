package com.example.castro.examen_uni2;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class NuevoUsuarioActivity extends AppCompatActivity {
    EditText apellido, nombre, contrasena, usuario;
    Button btnnuevo, btnguardar, btnborrar, btnabrir;
    ListView lstVwDatos;
    SQLiteDatabase sqlMiBD;
    long lReg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_usuario);

        apellido = (EditText) findViewById(R.id.apellido);
        nombre = (EditText) findViewById(R.id.nombre);
        contrasena = (EditText) findViewById(R.id.contrasena);
        usuario = (EditText) findViewById(R.id.usuario);
        btnnuevo = (Button) findViewById(R.id.btnnuevo);
        btnguardar = (Button) findViewById(R.id.btnguardar);
        btnborrar = (Button) findViewById(R.id.btnborrar);
        Button btn = (Button) findViewById(R.id.btnabrir);

       btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent1 = new Intent(v.getContext(), ArchivosAdapter.class);
                startActivityForResult(intent1, 0);
            }
        });
        //CREAMOS LA BASE DE DATOS(SI NO EXISTE) O LA ABRIMOS SI EXISTE
        lstVwDatos = (ListView) findViewById(R.id.lstVwDatos);
        sqlMiBD = openOrCreateDatabase("misdatos", MODE_PRIVATE, null);
        try {
            sqlMiBD.execSQL("create table tblDatos(" +
                    "miID integer PRIMARY KEY autoincrement," +
                    "nombre text," +
                    "apellido text"+
                    "usuario text);");
        } catch (SQLiteException E) {
            E.printStackTrace();
        }
        Cursor cDatos = sqlMiBD.rawQuery("select rowID as _id, "  + "nombre," +"apellido" +"usuario from tblDatos",null);
        lstVwDatos.setAdapter(new ArchivosAdapter(this, cDatos, 0));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 100) {
            // traer el usuario
        }
    }

    public void nuevo(View v) {
        apellido.setText("");
        nombre.setText("");
        contrasena.setText("");
        usuario.setText("");
    }

    public void guardar(View v) {
        String sApe= apellido.getText().toString();
        String sNom= nombre.getText().toString();
        String sUsu= usuario.getText().toString();
        int iPass=Integer.parseInt(contrasena.getText().toString());
//contenedor de valores
        ContentValues cvVal=new ContentValues();
        //clave--> columna, valor (tipo de dato primitivo)
        cvVal.put("nombre", sNom);
        cvVal.put("apellido",sApe);
        cvVal.put("usuario", sUsu);
        cvVal.put("pass",iPass);
        lReg=sqlMiBD.insert("tblDatos",null,cvVal);
        Toast.makeText(this, "Registro:" + lReg, Toast.LENGTH_LONG).show();
    }

    public void abrir(View v) {

        Intent intent = new Intent(this, UsuariosActivity.class);
        startActivityForResult(intent, 100);
    }

    public void onClickActua(View v){

        String sNom= nombre.getText().toString();
        String sApe= apellido.getText().toString();
        String sUsu= usuario.getText().toString();
        int iPass=Integer.parseInt(contrasena.getText().toString());

//contenedor de valores
        ContentValues cvVal=new ContentValues();
        //clave--> columna, valor (tipo de dato primitivo)
        cvVal.put("nombre", sNom);
        cvVal.put("apellido",sApe);
        cvVal.put("usuario", sUsu);
        cvVal.put("pass",iPass);
        //ACTUALIZAR LOS VALORES
        String sval=""+lReg;
        String[] sArgs={sval};
        sqlMiBD.update("tblDatos",cvVal,"rowID = ?", sArgs);


    }

    public void onClickDelete(View v){
        String sNom= nombre.getText().toString();
        String sApe= apellido.getText().toString();
        String sUsu= usuario.getText().toString();
        int iPass=Integer.parseInt(contrasena.getText().toString());
        ContentValues cvVal=new ContentValues();

        cvVal.put("nombre", sNom);
        cvVal.put("apellido",sApe);
        cvVal.put("usuario", sUsu);
        cvVal.put("pass",iPass);
        //borrar datos
        String sval=""+lReg;
        String[] sArgs={sval};
        lReg=sqlMiBD.delete("tblDatos",  "rowID > ? and rowID < ?", sArgs);
        Toast.makeText(this, "Registro Eliminado:" + lReg, Toast.LENGTH_LONG).show();
    }
}

