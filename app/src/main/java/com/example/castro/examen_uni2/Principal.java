package com.example.castro.examen_uni2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Principal extends AppCompatActivity {
    Button btnsalir;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        btnsalir = (Button) findViewById(R.id.btnsalir);
    }

    public void usuarios(View v) {

        Intent intent = new Intent (this, NuevoUsuarioActivity.class);
        startActivityForResult(intent, 0);
    }

    public void archivos(View v) {
        Intent intent = new Intent(this, ArchivosAdapter.class);
        startActivityForResult(intent, 1);
    }
}