package com.example.castro.examen_uni2;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class UsuariosActivity extends AppCompatActivity {
    ListView lstVwDatos;
    SQLiteDatabase sqlMiBD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuarios);
        lstVwDatos = (ListView) findViewById(R.id.lstVwDatos);
        Cursor cDatos = sqlMiBD.rawQuery("select rowID as _id, "  + "nombre," +"apellido" +"usuario from tblDatos",null);
        lstVwDatos.setAdapter(new ArchivosAdapter(this, cDatos, 0));
    }
}
